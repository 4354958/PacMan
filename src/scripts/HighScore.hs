-- for Json parsing
{-# LANGUAGE OverloadedStrings #-}

module HighScore where

import Data.Aeson
import Data.Aeson.Types
import Data.List (sort, sortBy, sortOn)
import Data.Ord (Down (..), comparing)
import Grid

newtype HighscoreData = HighscoreData [Highscore]
  deriving (Show)

data Highscore = Highscore
  { mapName :: String,
    scoreValues :: [Int]
  }
  deriving (Show)

instance FromJSON Highscore where
  parseJSON = withObject "Highscore" $ \v ->
    Highscore
      <$> v
      .: "map"
      <*> v
      .: "scores"

instance FromJSON HighscoreData where
  parseJSON = withObject "HighscoreData" $ \v ->
    HighscoreData
      <$> v
      .: "highscores"

instance ToJSON HighscoreData where
  toJSON (HighscoreData scores) = object ["highscores" .= map toHighscore scores]
    where
      toHighscore (Highscore mapName scores) = object ["map" .= mapName, "scores" .= scores]

-- If there are maps which don't exist in the highscores.json file, add them
addMissingMaps :: [(String, Grid)] -> HighscoreData -> HighscoreData
addMissingMaps mps (HighscoreData hs) = HighscoreData $ hs ++ map (`Highscore` []) missingMaps
  where
    missingMaps = filter (`notElem` namesFromJSON) namesFromMaps

    namesFromMaps = map fst mps
    namesFromJSON = map (\(Highscore nm sv) -> nm) hs

addScore :: HighscoreData -> String -> Int -> HighscoreData
addScore hsd@(HighscoreData hss) s i = HighscoreData (take index hss ++ [newhs] ++ drop (index + 1) hss)
  where
    (hs, index) = getHighScore hsd s 0
    newhs = hs {scoreValues = sortOn Down (i : scoreValues hs)}

getHighScore :: HighscoreData -> String -> Int -> (Highscore, Int)
getHighScore (HighscoreData []) _ _ = (Highscore "Nothing" [], 0)
getHighScore (HighscoreData (x : xs)) s i
  | mapName x == s = (x, i)
  | otherwise = getHighScore (HighscoreData xs) s (i + 1)