{-# LANGUAGE InstanceSigs #-}
{-# HLINT ignore "Use tuple-section" #-}
{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

module Grid where

import Data.Foldable (maximumBy)
import GHC.Float (int2Float)
import System.Random (Random (random), randomIO)

type Grid = [Row]

type Row = [GridBlock]

type GridLocation = (Int, Int)

type AbsoluteLocation = (Float, Float)

data GridBlock = Empty | Wall | PowerUp | Dot | Cherry | Door | Spawn | Prison | Special String
  deriving (Eq)

instance Show GridBlock where
  show Empty = " "
  show Wall = "#"
  show Dot = "."
  show PowerUp = "!"
  show Door = "="
  show Prison = " "
  show Spawn = " "
  show (Special c) = c

-- Used to turn a map.txt file into a Grid
readGrid :: String -> Grid
readGrid txt = map (map toGridBlock) (lines txt)

toGridBlock :: Char -> GridBlock
toGridBlock '#' = Wall
toGridBlock '.' = Dot
toGridBlock '!' = PowerUp
toGridBlock ' ' = Empty
toGridBlock '=' = Door
toGridBlock 'X' = Prison
toGridBlock 'P' = Spawn

-- | To keep the compiler happy
placeHolderGrid :: Grid
placeHolderGrid = [[]]

selectRandomGridLocation :: [GridLocation] -> IO GridLocation
selectRandomGridLocation xs = do
  randomNumber <- randomIO
  -- putStrLn $ "randomnumber is " ++ show randomNumber
  -- putStrLn $ "length of possible gridlocations " ++ show (length xs)
  let index = abs $ mod randomNumber $ length xs
  -- putStrLn $ "the modulated index " ++ show index
  return $ xs !! index

-- | returns the list of locations of all specified gridblocks in a grid
getAllLocationsOf :: Grid -> [GridBlock] -> [GridLocation]
getAllLocationsOf grid compareAgainst = [(y, x) | (x, row) <- zip [0 ..] grid, (y, block) <- zip [0 ..] row, block `elem` compareAgainst ]

-- | The type of GridBlock at a given location
getGridBlock :: Grid -> GridLocation -> GridBlock
getGridBlock g l@(b, r) = (g !! r) !! b

-- | Where the prison is located in the grid
getPrisonLocaction :: [Row] -> Int -> Maybe GridLocation
getPrisonLocaction [] _ = Nothing
getPrisonLocaction g@(x : xs) i = case findBlockInRow x 0 Door of
  Just n -> Just (n, i + 1)
  Nothing -> getPrisonLocaction xs (i + 1)

-- | Where pacMan should be spawned
getSpawnLocation :: [Row] -> Int -> Maybe GridLocation
getSpawnLocation [] _ = Nothing
getSpawnLocation g@(x : xs) i = case findBlockInRow x 0 Spawn of
  Just n -> Just (n, i)
  Nothing -> getSpawnLocation xs (i + 1)

-- | Find the location of a blocktype in a row
findBlockInRow :: Row -> Int -> GridBlock -> Maybe Int
findBlockInRow [] _ _ = Nothing
findBlockInRow (x : xs) i block
  | x == block = Just i
  | otherwise = findBlockInRow xs (i + 1) block

-- | The bool indicates whether the entity should be able to pass through doors
isWall :: Grid -> GridLocation -> Bool -> Bool
isWall g l False = case getGridBlock g l of
  Wall -> True
  Door -> True
  _ -> False
isWall g l True = case getGridBlock g l of
  Wall -> True
  _ -> False

gridWidth :: Grid -> Int
gridWidth = length . head

gridHeight :: Grid -> Int
gridHeight = length

-- | You can print a special character, at a specific location
printDebug :: Grid -> [(GridLocation, GridBlock)] -> String
printDebug g xs = printGrid (foldr (flip replaceInGrid) g xs)

-- | replaces the gridblock on GridLocation, and replaces it with the given gridblock
replaceInGrid :: Grid -> (GridLocation, GridBlock) -> Grid
replaceInGrid grid ((y, x), c)
{-| x < 0 || x > (length grid - 1) = grid -- check if the row index  is out of bounds
  | y < 0 || y > (length (head grid) - 1) = grid -- Check if column index is out of bounds
  | otherwise-}= take x grid ++ [replaceInRow (grid !! x) y c] ++ drop (x + 1) grid -- checks shouldn't be necessary but are useful while debugging

replaceInRow :: Row -> Int -> GridBlock -> Row
replaceInRow row index gb = case row !! index of
  Wall -> row
  _ -> take index row ++ [gb] ++ drop (index + 1) row

printGrid :: Grid -> String
printGrid = foldr (\x y -> printRow x ++ "\n" ++ y) []

printRow :: Row -> String
printRow = foldr (\x y -> show x ++ y) []

-- Correct the grid for inconsistencies
data ErrorMessage = Warning String | Fatal String

instance Show ErrorMessage where
  show :: ErrorMessage -> String
  show (Warning s) = "\ESC[33m" ++ s
  show (Fatal s) = "\ESC[31m" ++ s

correctGrid :: Grid -> (Grid, [ErrorMessage])
correctGrid g = detectSpawn $ detectPrison $ correctPortals $ correctRectangle (g, [])

-- Fill empty space in grid with walls to create rectangle
correctRectangle :: (Grid, [ErrorMessage]) -> (Grid, [ErrorMessage])
correctRectangle (g, errs)
  | p = (g, errs)
  | otherwise = (map (fillRow max) g, Warning "Corrected: Not a rectangle" : errs)
  where
    p = foldr (\x y -> length x == width && y) True g
    width = length $ head g
    max = maximum (map length g)

fillRow :: Int -> Row -> Row
fillRow max r
  | length r == max = r
  | otherwise = fillRow max (r ++ [Wall])

-- Fill portals without exit with Wall
correctPortals :: (Grid, [ErrorMessage]) -> (Grid, [ErrorMessage])
correctPortals (g@(x : xs), errs) = (newGrid, errs ++ errorStrings)
  where
    newGrid = foldr (\ip g -> replaceInGrid g (ip, Wall)) g illegalPortals
    errorStrings = [Warning ("Illegal portal corrected at " ++ x) | x <- map show illegalPortals]
    illegalPortals = findIllegalPortals g

findIllegalPortals :: Grid -> [GridLocation]
findIllegalPortals g@(x : xs) = findPortalExitsX leftPortals rightPortals ++ findPortalExitsY topPortals bottomPortals
  where
    topPortals = map (,0) (findPortalIndexes x 0 [])
    leftPortals = map (0,) (findPortalIndexes left 0 [])
    rightPortals = map (lastInRow,) (findPortalIndexes right 0 [])
    bottomPortals = map (,lastRow) (findPortalIndexes bottom 0 [])

    left = map head g
    right = map (!! lastInRow) g
    bottom = g !! lastRow

    lastInRow = gridWidth g - 1
    lastRow = gridHeight g - 1

findPortalIndexes :: [GridBlock] -> Int -> [Int] -> [Int]
findPortalIndexes [] _ ids = ids
findPortalIndexes (x : xs) id ids
  | x == Wall = findPortalIndexes xs (id + 1) ids
  | otherwise = findPortalIndexes xs (id + 1) (id : ids)

findPortalExitsX :: [GridLocation] -> [GridLocation] -> [GridLocation]
findPortalExitsX left right =
  [p | p <- left, all (\(_, y) -> y /= snd p) right]
    ++ [p | p <- right, all (\(_, y) -> y /= snd p) left]

findPortalExitsY :: [GridLocation] -> [GridLocation] -> [GridLocation]
findPortalExitsY top bottom =
  [p | p <- top, all (\(x, _) -> x /= fst p) bottom]
    ++ [p | p <- bottom, all (\(x, _) -> x /= fst p) top]

-- Detect whether there is a prison and spawn for pacman
detectPrison :: (Grid, [ErrorMessage]) -> (Grid, [ErrorMessage])
detectPrison (g, errs) = case getPrisonLocaction g 0 of
  Nothing -> (g, Fatal "Fatal: No prison detected" : errs)
  Just n -> (g, errs)

detectSpawn :: (Grid, [ErrorMessage]) -> (Grid, [ErrorMessage])
detectSpawn (g, errs) = case getSpawnLocation g 0 of
  Nothing -> (g, Fatal "Fatal: No spawn detected" : errs)
  Just n -> (g, errs)

-- | Find the distance between two gridLocations
distance :: GridLocation -> GridLocation -> Float
distance l1@(x1, y1) l2@(x2, y2) = sqrt $ sq (x2 - x1) + sq (y2 - y1)

absDistance :: AbsoluteLocation -> AbsoluteLocation -> Float
absDistance l1@(x1, y1) l2@(x2, y2) = sqrt $ fsq (x2 - x1) + fsq (y2 - y1)

sq :: Int -> Float
sq x = int2Float (x * x)

fsq :: Float -> Float
fsq x = x * x