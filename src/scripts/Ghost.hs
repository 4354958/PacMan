module Ghost where

import Data.List (maximumBy, minimumBy, sort, sortBy)
import Data.Maybe (catMaybes, fromMaybe, mapMaybe)
import Data.Ord (comparing)
import GHC.Float (int2Float)
import Graphics.Gloss.Data.Vector (rotateV)
import Grid
import Movement

type Target = GridLocation

type IsEaten = Bool

type ScatterSquare = GridLocation

data Name = BLINKY | CLYDE | PINKY | INKY

instance Show Name where
  show BLINKY = "blinky"
  show CLYDE = "clyde"
  show PINKY = "pinky"
  show INKY = "inky"

data Ghost = Ghost -- this and pacman are suspiciously object oriented
  { name :: Name,
    _currentLocation :: GridLocation,
    target :: Target,
    _absoluteLocation :: AbsoluteLocation,
    currentDirection :: CurrentDirection,
    scatterSquare :: ScatterSquare,
    _ghostMode :: GhostMode,
    _isEaten :: IsEaten
  }

data GhostMode = Scatter | Chase

instance Entity Ghost where
  updateGridLocation g gh = gh {_currentLocation = getNextGridLocation, currentDirection = nd, _absoluteLocation = newAbsLocation}
    where
      nd = directionFromLocations g cl getNextGridLocation

      getNextGridLocation = bestMove t moves
      cl = _currentLocation gh

      moves = getMoves g gh
      t = target gh
      newAbsLocation = loc2AbsLoc cl
  getGridLocation = _currentLocation
  getDirection = currentDirection
  getAbsLocation = _absoluteLocation
  setAbsLocation gh absLocation = gh {_absoluteLocation = absLocation}

bestMove :: Target -> [GridLocation] -> GridLocation
bestMove t = minimumBy (comparing (distance t))

getMoves :: Grid -> Ghost -> [GridLocation]
getMoves g gh = noMoves $ mapMaybe (tryMoveInDirection g cl allowedToPassThroughDoor . Just) legalDirections
  where
    noMoves ls
      | null ls = [fromMaybe cl (tryMoveInDirection g cl allowedToPassThroughDoor (turnAround cd))]
      | otherwise = ls

    legalDirections = getLegalDirections cd
    allowedToPassThroughDoor = ie || cd == Just N

    ie = _isEaten gh
    cl = _currentLocation gh
    cd = currentDirection gh

    getLegalDirections :: Maybe Direction -> [Direction]
    getLegalDirections Nothing = [N, E, W, S]
    getLegalDirections (Just N) = [N, E, W]
    getLegalDirections (Just W) = [N, W, S]
    getLegalDirections (Just E) = [N, E, S]
    getLegalDirections (Just S) = [E, W, S]

-- When in prison, the target is set kept the same, the target will be set in the controller
-- We don't want to give pacman as argument because we would need to import pacman
setTarget :: Grid -> GridLocation -> GridLocation -> CurrentDirection -> Ghost -> Ghost
setTarget g pacManLocation bl pmd gh = case inPrison g gh of
  Just gh -> gh
  Nothing -> case ghostName of
    BLINKY -> gh {target = pacManLocation}
    CLYDE -> gh {target = clydeTarget}
    INKY -> gh {target = inkyTarget}
    PINKY -> gh {target = pinkyTarget}
    where
      ghostName = name gh
      gl = _currentLocation gh
      scs = scatterSquare gh
      pinkyTarget = getPinkyTarget g pacManLocation pmd
      inkyTarget = getInkyTarget g pacManLocation bl
      clydeTarget = getClydeTarget g gl pacManLocation scs
      -- To find PM future location, 4 gridblocks in front of his current location
      -- When that square is out of bounds, location is the edge of the _grid
      -- When PacMan stands still, the location is N of him
      getPinkyTarget :: Grid -> GridLocation -> Maybe Direction -> GridLocation
      getPinkyTarget g gl@(x, y) d = case d of
        Just S -> if y + 4 > height then (x, height) else (x, y + 4)
        Just W -> if x - 4 < 0 then (0, y) else (x - 4, y)
        Just E -> if x + 4 > width then (width, y) else (x + 4, y)
        _ -> if y - 4 < 0 then (x, 0) else (x, y - 4)
        where
          height = length g - 1
          width = length (head g) - 1
      -- This is a weird one
      -- The vector from PacMan's location to Blinky's location
      -- Rotated 180 degrees, is the location of Inky's target
      getInkyTarget :: Grid -> GridLocation -> GridLocation -> GridLocation
      getInkyTarget g pacManLocation@(x1, y1) bl@(x2, y2) = addVectors pacManLocation vector -- pacManLocation could be removed from the left side here
        where
          vector = rotateV pi (int2Float (x2 - x1), int2Float (y2 - y1))
          addVectors (x1, y1) (xv, yv) = (x1 + round xv, y1 + round yv)
      getClydeTarget :: Grid -> GridLocation -> GridLocation -> ScatterSquare -> GridLocation
      getClydeTarget g l pacManLocation scs
        | distance l pacManLocation > 8 = pacManLocation
        | otherwise = scs

isInPrison :: Grid -> Ghost -> Bool
isInPrison grid ghost = getGridBlock grid gl == Prison
  where
    gl@(x, y) = _currentLocation ghost

inPrison :: Grid -> Ghost -> Maybe Ghost
inPrison g gh = case getGridBlock g gl of
  Prison -> Just gh {target = (x, y - 2)}
  Door -> Just gh {target = (x, y - 1)}
  _ -> Nothing
  where
    gl@(x, y) = _currentLocation gh

scatter :: Grid -> Ghost -> Ghost
scatter g gh = fromMaybe gh {target = scs} (inPrison g gh)
  where
    scs = scatterSquare gh

avoid :: GridLocation -> Ghost -> Ghost
avoid pacManLocation@(x2, y2) ghost@Ghost {_currentLocation = (x1, y1)} = ghost {target = addVectors pacManLocation vector}
  where
    vector = rotateV pi (int2Float (x2 - x1), int2Float (y2 - y1))
    addVectors (x1, y1) (xv, yv) = (x1 + round xv, y1 + round yv)

getBlinky :: [Ghost] -> Ghost
getBlinky [] = defaultBlinky (0, 0)
getBlinky (gh : _ghosts) = case name gh of
  BLINKY -> gh
  _ -> getBlinky _ghosts

defaultClyde :: GridLocation -> Ghost
defaultClyde l =
  Ghost
    { name = CLYDE,
      _currentLocation = l,
      target = (100, 100),
      _absoluteLocation = loc2AbsLoc l,
      currentDirection = Nothing,
      scatterSquare = (100, 100),
      _ghostMode = Scatter,
      _isEaten = False
    }

defaultBlinky :: GridLocation -> Ghost
defaultBlinky l =
  Ghost
    { name = BLINKY,
      _currentLocation = l,
      target = (100, 100),
      _absoluteLocation = loc2AbsLoc l,
      currentDirection = Nothing,
      scatterSquare = (0, 100),
      _ghostMode = Scatter,
      _isEaten = False
    }

defaultInky :: GridLocation -> Ghost
defaultInky l =
  Ghost
    { name = INKY,
      _currentLocation = l,
      target = (100, 100),
      _absoluteLocation = loc2AbsLoc l,
      currentDirection = Nothing,
      scatterSquare = (100, 0),
      _ghostMode = Scatter,
      _isEaten = False
    }

defaultPinky :: GridLocation -> Ghost
defaultPinky l =
  Ghost
    { name = PINKY,
      _currentLocation = l,
      target = (100, 100),
      _absoluteLocation = loc2AbsLoc l,
      currentDirection = Nothing,
      scatterSquare = (0, 0),
      _ghostMode = Scatter,
      _isEaten = False
    }
