{-# LANGUAGE NamedFieldPuns #-}

-- | This module defines how the state changes
--   in response to time and user input
module Controller where

import Control.Exception (handle)
import Data.Aeson (Value (Bool))
import Data.List (minimumBy)
import Data.Maybe (fromMaybe)
import GHC.Base (maxInt)
import GHC.Float (int2Float)
import Ghost
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Grid
import HighScore
import Load
import Model
import Movement
import PacMan
import Text.ParserCombinators.ReadP (char)

-- | Handle one iteration of the game
step :: Float -> GameState -> IO GameState
step timeSinceLastFrame gstate@GameState {_grid = g, _images = imgs, _prisonLocation = pl, _pause = p, _lives = lvs, _eatingEvent = eatingEvent} = do
  -- putStrLn $ "HasCherry is: " ++ show (_generatedCherry gstate)
  gstate <-
    if mapSelected $ _selectState gstate -- if map has been selected
      then
        if _generatedCherry gstate -- if theres a cherry already
          then return gstate -- continue
          else addCherryToState gstate {_generatedCherry = True} -- else generate a cherry in the level
      else return gstate {_pause = Pause} -- continue the map selection process
  gstate <- case p of
    Pause -> return gstate
    Play -> case event of
      NoEvent -> processFrame gstate
      _ -> return $ handleFreezeFrame gstate -- freeze animation with possibility to do something
  gstate <- if _isEnabled eatingEvent then return gstate {_eatingEvent = updateEatingMode eatingEvent} else return gstate
  handleContinuousEvents gstate
  where
    (FreezeEvent event i) = _freezeEvent gstate
    sl = _pmSpawnLocation gstate
    wSize = _windowSize gstate
    ss = _selectState gstate
    sc = _score gstate

    processFrame :: GameState -> IO GameState
    processFrame gstate
      | aniFrameCount < 8 = do
          gstate <- return $ animationFrame gstate -- move absolute position of entities, but not gridlocation
          gstate <- collisionHandler gstate -- collision between entities
          continuousPlayEvents gstate timeSinceLastFrame
      | otherwise = do
          gstate <- return $ resetAnimationCounter gstate
          gstate <- return $ gridBlockHandler gstate -- collision between pacman and items on the ground
          gstate <- return $ setGhostsMode gstate -- handles actions dependent on elapsed time (flips between chasemodes atm)
          gstate <- updateEntities gstate -- updates gridlocations of entities (finished one cycle) and update targets for ghosts
          gstate <- checkIfWon gstate
          continuousPlayEvents gstate timeSinceLastFrame -- update elapsed time
      where
        aniFrameCount = _animationFrameCount gstate

    -- 'animates' positions of entities
    animationFrame :: GameState -> GameState
    animationFrame gstate =
      gstate
        { _animationFrameCount = aniFrameCount + 1,
          _pacMan = setAbsLocation pacMan (moveAbsolute gstate pacMan),
          _ghosts = newGhs
        }
      where
        aniFrameCount = _animationFrameCount gstate
        pacMan = _pacMan gstate
        allGhosts = _ghosts gstate
        pacManAbsLoc = moveAbsolute gstate pacMan
        ghostAbsLoc = map (moveAbsolute gstate) allGhosts
        newGhs = map (\gh -> setAbsLocation gh (moveAbsolute gstate gh)) allGhosts

    moveAbsolute :: (Entity a) => GameState -> a -> AbsoluteLocation
    moveAbsolute gstate entity = case cd of
      Nothing -> (x, y)
      Just N -> (x, y + 1)
      Just S -> (x, y - 1)
      Just W -> (x - 1, y)
      Just E -> (x + 1, y)
      where
        cd = getDirection entity
        (x, y) = getAbsLocation entity
        i = _animationFrameCount gstate

    -- \| Handle collisions
    -- Based on the absolute positions of the _ghosts and pacman
    collisionHandler :: GameState -> IO GameState
    collisionHandler gstate
      | collision =
          if _isEnabled $ _eatingEvent gstate
            then
              if _isEaten closestGhost
                then return gstate
                else return $ ghostEatenHandler gstate closestGhost
            else
              if _isEaten closestGhost
                then return gstate
                else deathHandler gstate
      | otherwise = return gstate
      where
        allGhosts = _ghosts gstate
        pacManAbsLoc = getAbsLocation (_pacMan gstate)
        ghostAbsLoc = map getAbsLocation allGhosts
        distances = map (absDistance pacManAbsLoc) ghostAbsLoc
        closestDistance = minimum distances
        collision = closestDistance < 4
        distancesWithGhosts = zip distances allGhosts
        closestPair = minimumBy (\(dist1, _) (dist2, _) -> compare dist1 dist2) distancesWithGhosts
        closestGhost = snd closestPair

    ghostEatenHandler :: GameState -> Ghost -> GameState
    ghostEatenHandler gstate closestGhost = (modifyScore (causeFreezeEvent gstate GhostEaten 30) 200) {_ghosts = adjustGhosts eatGhost closestGhost (_ghosts gstate)} -- cause freeze, set iseaten to tru
      where
        eatGhost gh = gh {_isEaten = True}

    deathHandler :: GameState -> IO GameState
    deathHandler gstate
      | lvs > 0 = return $ causeFreezeEvent gstate PacManDeath 45
      | otherwise = gameOverHandler gstate
      where
        lvs = _lives gstate

    -- \| Handles what happens when pacman reaches a square
    gridBlockHandler :: GameState -> GameState
    gridBlockHandler gstate = case getGridBlock g l of
      Cherry -> flip toEmpty l $ modifyScore gstate 1000
      Dot -> flip toEmpty l $ modifyScore gstate 100
      PowerUp -> enableEatingmode $ flip toEmpty l $ modifyScore gstate 500
      _ -> gstate
      where
        l = getGridLocation $ _pacMan gstate
        g = _grid gstate
        enableEatingmode gstate = gstate {_eatingEvent = EatingEvent True (45 * 5)} -- this makes the eatingevent last 5 seconds on 45fps
        toEmpty gstate gl = gstate {_grid = replaceInGrid (_grid gstate) (gl, Empty)}

    resetAnimationCounter :: GameState -> GameState
    resetAnimationCounter gstate = gstate {_animationFrameCount = 0}

    -- \| Updates PacMan's location
    setGhostsMode :: GameState -> GameState
    setGhostsMode gstate = gstate {_ghosts = ghostmode ela gstate}
      where
        ela = _elapsedTime gstate
        ghostmode time gstate
          | timeFrame 0 4 time = map (`setMode` Scatter) $ _ghosts gstate
          | timeFrame 4 8 time = map (`setMode` Chase) $ _ghosts gstate
          | timeFrame 8 13 time = map (`setMode` Scatter) $ _ghosts gstate
          | timeFrame 13 20 time = map (`setMode` Chase) $ _ghosts gstate
          | timeFrame 20 26 time = map (`setMode` Scatter) $ _ghosts gstate
          | time > 26 = map (`setMode` Chase) $ _ghosts gstate
          where
            setMode gh ghostmode = gh {_ghostMode = ghostmode}

    updateEntities :: GameState -> IO GameState -- update after animation
    updateEntities gstate = do
      gstate <- return $ updatePacMan gstate
      updateGhosts gstate

    updatePacMan :: GameState -> GameState
    updatePacMan gstate = gstate {_pacMan = updateGridLocation (_grid gstate) (_pacMan gstate)}

    -- \| Update ghost locations
    updateGhosts :: GameState -> IO GameState
    updateGhosts gstate = do
      gstate <- return $ gstate {_ghosts = map (updateGridLocation (_grid gstate)) (_ghosts gstate)}
      gstate <- return $ unEatPrisonGhosts gstate
      return $ updateTargets gstate

    unEatPrisonGhosts :: GameState -> GameState
    unEatPrisonGhosts gstate@GameState {_grid, _ghosts} = gstate {_ghosts = map (unEat _grid) _ghosts}
      where
        unEat grid ghost = if isInPrison grid ghost then ghost {_isEaten = False} else ghost
    updateTargets :: GameState -> GameState
    updateTargets gstate = gstate {_ghosts = map (updateTarget gstate) ghosts}
      where
        ghosts = _ghosts gstate

    updateTarget :: GameState -> Ghost -> Ghost
    updateTarget gstate ghost
      | _isEaten ghost = ghost {target = _prisonLocation gstate} -- if the ghost is eaten goto prison
      | _isEnabled $ _eatingEvent gstate = avoid pml ghost -- if pacman is eating -> avoid
      | otherwise -- otherwise follow previously set ai pattern
        =
          case _ghostMode ghost of
            Scatter -> scatter grid ghost
            Chase -> setTarget grid pml bl pmd ghost
      where
        pml = getGridLocation (_pacMan gstate)
        pmd = getDirection (_pacMan gstate)
        blinky = getBlinky (_ghosts gstate)
        bl = getGridLocation blinky
        grid = _grid gstate

    checkIfWon :: GameState -> IO GameState
    checkIfWon gstate@GameState {_grid = grid} =
      if not $ any (Dot `elem`) grid
        then gameWonHandler gstate
        else return gstate

    continuousPlayEvents :: GameState -> Float -> IO GameState
    continuousPlayEvents gstate timeSinceLastFrame = do
      return $ updateElapsedTime gstate timeSinceLastFrame

    updateElapsedTime :: GameState -> Float -> GameState
    updateElapsedTime gstate timeSinceLastFrame = gstate {_elapsedTime = _elapsedTime gstate + timeSinceLastFrame}

    -- \| What to do after a certain elapsed time
    handleFreezeFrame :: GameState -> GameState
    handleFreezeFrame
      gstate@GameState
        { _frameCounter,
          _freezeEvent,
          _grid,
          _images,
          _prisonLocation,
          _windowSize,
          _selectState,
          _pmSpawnLocation,
          _score,
          _lives,
          _highScoreData
        }
        | i > 0 = case event of
            GameOver -> gstate {_freezeEvent = FreezeEvent event (i - 1)}
            GameWon -> gstate {_freezeEvent = FreezeEvent event (i - 1)}
            _ -> gstate {_freezeEvent = FreezeEvent event (i - 1)}
        | i == 0 = case event of
            PacManDeath ->
              (initialState _grid _images _windowSize _selectState _prisonLocation _pmSpawnLocation _highScoreData True) {_lives = _lives - 1, _score = _score}
            _ -> gstate {_freezeEvent = FreezeEvent NoEvent 0}

    updateEatingMode :: EatingEvent -> EatingEvent
    updateEatingMode eatingEvent
      | not $ _isEnabled eatingEvent = EatingEvent False 0 -- failsafe
      | _framesLeft eatingEvent == 0 = EatingEvent False 0
      | otherwise = eatingEvent {_framesLeft = _framesLeft eatingEvent - 1}

    handleContinuousEvents :: GameState -> IO GameState -- actions that have to happen each frame of the program
    handleContinuousEvents gstate = do
      return $ updateFrameCounter gstate

    updateFrameCounter :: GameState -> GameState
    updateFrameCounter gstate = gstate {_frameCounter = _frameCounter gstate + 1}

    -- \| Whether the elapsed time is between a range
    timeFrame :: Float -> Float -> Float -> Bool
    timeFrame b1 b2 secs = secs > b1 && secs <= b2

    adjustGhosts :: (Ghost -> Ghost) -> Ghost -> [Ghost] -> [Ghost] -- changes one ghost in the ghost list based on the function
    adjustGhosts f targetGhost ghosts = take index ghosts ++ f (ghosts !! index) : drop (index + 1) ghosts
      where
        index = case name targetGhost of -- very smelly
          BLINKY -> 0
          CLYDE -> 1
          PINKY -> 2
          INKY -> 3

    gameOverHandler :: GameState -> IO GameState
    gameOverHandler gstate = updateScores $ causeFreezeEvent gstate GameOver maxInt

    gameWonHandler :: GameState -> IO GameState
    gameWonHandler gstate = updateScores $ causeFreezeEvent gstate GameWon maxInt

    updateScores :: GameState -> IO GameState
    updateScores gstate = do
      let
      saveHighScores updatedHighScores
      return $ gstate {_highScoreData = updatedHighScores}
      where
        updatedHighScores = addScore hsdata (getMapName sstate) score
        sstate = _selectState gstate
        score = _score gstate
        hsdata = _highScoreData gstate

    -- \| Print _grid in the console
    consoleDebug :: GameState -> IO ()
    consoleDebug gstate = do
      print pacManLocation
      print (nextDirection (_pacMan gstate))
      putStr (printDebug (_grid gstate) [(pacManLocation, Special "●")])
      where
        pacManLocation = getGridLocation (_pacMan gstate)