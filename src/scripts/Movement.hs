module Movement where

import GHC.Float (int2Float)
import Graphics.Gloss.Data.Vector
import Grid

data Direction = N | E | S | W
  deriving (Show, Eq, Ord)

type NextDirection = Maybe Direction

type CurrentDirection = Maybe Direction

class Entity a where
  updateGridLocation :: Grid -> a -> a
  getGridLocation :: a -> GridLocation
  getDirection :: a -> Maybe Direction
  getAbsLocation :: a -> AbsoluteLocation
  setAbsLocation :: a -> AbsoluteLocation -> a

-- | Gives GridLocation based on a current Location and Direction
-- Also handles wrap-around clauses
-- When entities reach the end of the _grid, they wrap around
getNextGridLocation :: Grid -> GridLocation -> Direction -> GridLocation
getNextGridLocation g (0, y) W = (length (head g) - 1, y)
getNextGridLocation g (x, 0) N = (x, length g - 1)
getNextGridLocation _ (x, y) N = (x, y - 1)
getNextGridLocation _ (x, y) W = (x - 1, y)
getNextGridLocation g (x, y) S
  | y == length g - 1 = (x, 0)
  | otherwise = (x, y + 1)
getNextGridLocation g (x, y) E
  | x == length (head g) - 1 = (0, y)
  | otherwise = (x + 1, y)

-- | If a updateGridLocation is possible, it returns the new location, else Nothing.
-- Also handles the case that there is a door.
-- Pacman cannot go through the door, _ghosts only under cetrain conditions (this logic is found in ghost.hs).
tryMoveInDirection :: Grid -> GridLocation -> Bool -> Maybe Direction -> Maybe GridLocation
tryMoveInDirection _ _ _ Nothing = Nothing
tryMoveInDirection g gl isEaten (Just d)
  | isWall g nl (isEaten || d == N) = Nothing
  | otherwise = Just nl
  where
    nl = getNextGridLocation g gl d

-- | Finds the direction that was moved in previously,
--  based on the currect location and previous location
-- Accounts for portals, in which case the direction is actually opposite
directionFromLocations :: Grid -> GridLocation -> GridLocation -> Maybe Direction
directionFromLocations g cl@(x1, y1) prevLoc@(x2, y2)
  | x1 > x2 = checkPortal $ Just W
  | x1 < x2 = checkPortal $ Just E
  | y1 < y2 = checkPortal $ Just S
  | y1 > y2 = checkPortal $ Just N
  | otherwise = Nothing
  where
    portal = distance prevLoc cl > 1
    checkPortal d
      | portal = turnAround d
      | otherwise = d

-- | Simply returns the opposite direction
turnAround :: Maybe Direction -> Maybe Direction
turnAround Nothing = Nothing
turnAround (Just N) = Just S
turnAround (Just S) = Just N
turnAround (Just W) = Just E
turnAround (Just E) = Just W

loc2AbsLoc :: GridLocation -> AbsoluteLocation
loc2AbsLoc l@(x, y) = (absX, absY)
  where
    absX = int2Float x * 8
    absY = -int2Float y * 8