-- | This module defines how to turn
--   the game state into a picture
module View where

import Data.Maybe
import GHC.Float (float2Int, int2Float)
import Ghost
import Graphics.Gloss
import Grid
import HighScore (getHighScore, scoreValues)
import Model
import Movement
import PacMan

view :: GameState -> IO Picture
view gstate =
  if mapSelected $ _selectState gstate
    then
      ( do
          case _pause gstate of
            Pause -> return $ pictures [gameView, viewPausedText gstate]
            Play -> return gameView
      )
    else return $ viewMapSelector gstate
  where
    gameView = getPictures gstate
    viewPausedText :: GameState -> Picture
    viewPausedText gstate = Translate (-180.0) 0 $ Scale 0.6 0.6 $ viewBlinkingText gstate "PAUSED"
    -- function that decides which pictures to show based on the current freeze event
    getPictures :: GameState -> Picture
    getPictures gstate = case event of
      GameOver ->
        pictures $
          map (scaleToFit gstate . defaultCenter g) [v_ghosts, vPacMan]
            ++ [viewHighScores gstate, viewGameOverText gstate "Game Over!"] -- overlay these
      GameWon ->
        pictures $
          map (scaleToFit gstate . defaultCenter g) [v_ghosts, vPacMan]
            ++ [viewHighScores gstate, viewGameOverText gstate "You Win!"]
      _ -> defaultPics
      where
        -- views
        vGrid = viewGrid gstate
        vScore = viewScore gstate
        v_ghosts = viewGhosts gstate
        vPacMan = viewPacman gstate
        vLives = viewLives gstate lvs
        vTargets = viewTargetsDebug gstate
        vDefault = [vGrid g, vScore, v_ghosts, vPacMan, bottom vLives]

        -- scaling and transforming
        bottom = Translate 0.0 (int2Float $ -gridHeight g * 8)
        defaultPics = scaleToFit gstate $ defaultCenter g $ pictures vDefault
        -- other
        (FreezeEvent event i) = _freezeEvent gstate
        lvs = _lives gstate
        g = _grid gstate

        viewHighScores :: GameState -> Picture
        viewHighScores gstate =
          pictures $
            highScoreLabel (-100) highscoreHeight
              : maybeNewHighScore (-75) (highscoreHeight - 30)
              : allScorePics
          where
            -- local vars
            highscoreHeight = -25
            mapName = getMapName $ _selectState gstate
            hsdata = _highScoreData gstate
            (highscore, _) = getHighScore hsdata mapName 0
            scores = scoreValues highscore
            myScore = _score gstate

            -- functions
            highScoreLabel transX transY = Translate transX transY (Scale 0.3 0.3 $ color white $ text "Highscores:")
            maybeNewHighScore transX transY = if head scores == myScore then newHighScoreText transX transY else blank
            newHighScoreText transX transY = Translate transX transY $ Scale 0.15 0.15 $ color yellow $ text "New highscore!"
            allScorePics = zipWith scorePic [1 ..] (take 10 scores)
            scorePic index score =
              translate (-75) (fromIntegral (-20 * index) + highscoreHeight - 40) $
                scale 0.1 0.1 $
                  clr score $
                    text (show index ++ ": " ++ show score)
              where
                clr i = if i == myScore then color red else color white
        -- GameOverScree
        viewGameOverText :: GameState -> String -> Picture
        viewGameOverText gstate string =
          pictures
            [ Translate (-220) 150 $ Scale 0.65 0.65 $ color white $ text string,
              Translate (-210) 60 $ Scale 0.2 0.2 $ viewBlinkingText gstate "Press r to restart or q to exit"
            ]

    viewMapSelector :: GameState -> Picture
    viewMapSelector gstate = pictures [_grid, titleTxt, nameTxt, arrL, arrR, selectTxt]
      where
        s = _selectState gstate

        _grid = Translate 0 0 $ defaultCenter g $ viewGrid gstate (getGrid s)
        titleTxt = Translate (-115) 260 $ Scale 0.3 0.3 $ color white $ text "Map Selection"
        nameTxt = Translate nameLength (gridHeight + 20) $ Scale 0.15 0.15 $ color red $ text m
        arrL = Translate (-arrowDistance) 0 $ Rotate 180 selectArrow
        arrR = Translate arrowDistance (-36) selectArrow
        selectTxt = Translate (-105) (-260) $ Scale 0.15 0.15 $ viewBlinkingText gstate "Press Enter to select"

        g = getGrid s
        m = getMapName s
        nameLength = int2Float $ -5 * length m
        gridHeight = int2Float $ length g * 4
        gridWidth = length (head g) * 4
        arrowDistance = int2Float $ max 250 (gridWidth + 20)

        selectArrow :: Picture
        selectArrow = scale 4.0 4.0 $ color white $ Polygon [(0, 0), (4, 4), (0, 8)]

defaultCenter :: Grid -> Picture -> Picture
defaultCenter g = Translate (-int2Float (gridWidth g) * 4) (int2Float (gridHeight g) * 4)

-- Scales as large as possible without leaving the screen
-- leaves 100 px margin at the top and 200 px at the bottom
scaleToFit :: GameState -> Picture -> Picture
scaleToFit gstate = Scale scaleValue scaleValue
  where
    scaleValue = min (xMax / width) (yMax / height)
    width = int2Float $ gridWidth g * 8
    height = int2Float $ gridHeight g * 8
    g = _grid gstate
    xMax = int2Float $ x - 100
    yMax = int2Float $ y - 200
    (x, y) = _windowSize gstate

viewGrid :: GameState -> Grid -> Picture
viewGrid gstate g = pictures $ zipWith translateAccordingToIndex [0 ..] g
  where
    translateAccordingToIndex i r = Translate 0.0 (i * (-8.0)) (viewRow gstate r)

viewRow :: GameState -> Row -> Picture
viewRow gstate r = pictures $ zipWith translateAccordingToIndex [0 ..] r
  where
    translateAccordingToIndex i gb = Translate (i * 8.0) 0.0 (viewGridBlock gstate gb)

viewGridBlock :: GameState -> GridBlock -> Picture
viewGridBlock gstate gb = case gb of
  Wall -> color blue $ polygon [(0.0, 0.0), (0.0, -8.0), (8.0, -8.0), (8.0, 0.0)]
  Dot -> centerImg $ fromMaybe notFound (lookup "dot" (_images gstate))
  PowerUp -> if slowFrames gstate then centerImg $ fromMaybe notFound (lookup "powerup" (_images gstate)) else blank
  Door -> color white $ polygon [(0.0, 0.0), (0.0, -8.0), (8.0, -8.0), (8.0, 0.0)]
  Cherry -> centerImg $ scale 0.5 0.5 $ fromMaybe notFound (lookup "cherry" (_images gstate)) -- scale 0.5 because the bmp is 12x12 instead of 8x8
  _ -> blank

viewConsoleMode :: GameState -> [Picture]
viewConsoleMode gstate =
  [ viewNextDirection gstate,
    translate 0.0 100.0 (viewScore gstate)
  ]

viewNextDirection :: GameState -> Picture
viewNextDirection gstate = color green (text $ show $ nextDirection (_pacMan gstate))

viewScore :: GameState -> Picture
viewScore gstate = Scale 0.1 0.1 $ color white (text $ "Score: " ++ show (_score gstate))

viewLives :: GameState -> Int -> Picture
viewLives _ 0 = blank
viewLives gstate i = pictures [centerImg $ transl $ scaleEntity img, viewLives gstate (i - 1)]
  where
    imgs = _images gstate
    img = fromMaybe notFound (lookup "pacman1" imgs)
    transl = Translate (int2Float i * 8) 0.0

viewPacman :: GameState -> Picture
viewPacman gstate = centerImg $ translateEntity gstate pacMan $ scaleEntity $ rot pmImg
  where
    pacMan = _pacMan gstate
    cd = getDirection pacMan
    imgs = _images gstate
    pmImg
      | fastFrames gstate = fromMaybe notFound (lookup "pacman2" imgs)
      | otherwise = fromMaybe notFound (lookup "pacman1" imgs)
    rot img = case cd of
      Nothing -> fromMaybe notFound (lookup "pacmanStill" imgs)
      Just N -> Rotate (-90) img
      Just S -> Rotate 90 img
      Just W -> Rotate 180 img
      Just E -> img

viewGhosts :: GameState -> Picture
viewGhosts gstate = pictures $ map (viewGhost gstate) (_ghosts gstate)

viewGhost :: GameState -> Ghost -> Picture
viewGhost gstate gh = centerImg $ translateEntity gstate gh $ scaleEntity ghostImg
  where
    imgs = _images gstate
    nmStr = show $ name gh
    mode = _ghostMode gh
    eyes = viewEyes gstate gh
    ghostImg
      | _isEaten gh = eyes
      | _isEnabled $ _eatingEvent gstate = scaredGhost gstate
      | otherwise = pictures [normalGhost gstate, eyes]
      where
        normalGhost :: GameState -> Picture
        normalGhost gstate
          | slowFrames gstate = fromMaybe notFound (lookup (nmStr ++ "1") imgs)
          | otherwise = fromMaybe notFound (lookup (nmStr ++ "2") imgs)
        scaredGhost :: GameState -> Picture
        scaredGhost gstate = if lowFramesLeft $ _eatingEvent gstate then blinkingTimeScaredGhost gstate else normalScaredGhost gstate
          where
            lowFramesLeft :: EatingEvent -> Bool
            lowFramesLeft (EatingEvent enabled durationLeft)
              | not enabled = False
              | durationLeft < 90 = True
              | otherwise = False
            normalScaredGhost :: GameState -> Picture
            normalScaredGhost gstate
              | slowFrames gstate = fromMaybe notFound (lookup "eat_g_1" imgs)
              | otherwise = fromMaybe notFound (lookup "eat_g_2" imgs)
            blinkingTimeScaredGhost :: GameState -> Picture
            blinkingTimeScaredGhost gstate = if verySlowFrames gstate then timeGhost gstate else normalScaredGhost gstate
              where
                timeGhost :: GameState -> Picture
                timeGhost gstate
                  | slowFrames gstate = fromMaybe notFound (lookup "eat_time_1" imgs)
                  | otherwise = fromMaybe notFound (lookup "eat_time_2" imgs)

viewEyes :: GameState -> Ghost -> Picture
viewEyes GameState {_images = images} gh = case cd of
  Nothing -> fromMaybe notFound (lookup "eyesE" images)
  Just N -> fromMaybe notFound (lookup "eyesN" images)
  Just S -> fromMaybe notFound (lookup "eyesS" images)
  Just W -> fromMaybe notFound (lookup "eyesW" images)
  Just E -> fromMaybe notFound (lookup "eyesE" images)
  where
    cd = getDirection gh

scaleEntity :: Picture -> Picture
scaleEntity = Scale 0.57 0.57

translateEntity :: (Entity a) => GameState -> a -> Picture -> Picture
translateEntity gstate entity = Translate xAbs yAbs
  where
    p = _pause gstate
    (x, y) = getGridLocation entity
    (xAbs, yAbs) = getAbsLocation entity

viewTargetsDebug :: GameState -> Picture
viewTargetsDebug gstate = pictures $ map (viewTarget gstate) (_ghosts gstate)

viewTarget :: GameState -> Ghost -> Picture
viewTarget gstate gh = transl $ clr box
  where
    nm = name gh
    t@(x, y) = target gh
    ie = _isEaten gh
    clr = case nm of
      CLYDE -> color orange
      INKY -> color cyan
      BLINKY -> color red
      PINKY -> color magenta
    box = polygon [(0.0, 0.0), (0.0, -4.0), (4.0, -4.0), (4.0, 0.0)]
    transl = Translate (int2Float x * 8) (-int2Float y * 8)

viewBlinkingText :: GameState -> String -> Picture
viewBlinkingText state str
  | verySlowFrames state = color white $ text str
  | otherwise = blank

notFound :: Picture
notFound = color green $ polygon [(0.0, 0.0), (0.0, -8.0), (8.0, -8.0), (8.0, 0.0)]

verySlowFrames :: GameState -> Bool
verySlowFrames gstate = _frameCounter gstate `mod` 32 < 16

slowFrames :: GameState -> Bool
slowFrames gstate = _frameCounter gstate `mod` 18 < 9

fastFrames :: GameState -> Bool
fastFrames gstate = _frameCounter gstate `mod` 6 < 3

centerImg :: Picture -> Picture
centerImg = Translate 4.0 (-4.0)