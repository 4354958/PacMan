{-# LANGUAGE NamedFieldPuns #-}

module Input where

import Data.Maybe (fromMaybe)
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Grid
import Model
import Movement
import PacMan
import System.Exit (exitSuccess)

-- \| Handle user input
input :: Event -> GameState -> IO GameState
input (EventResize newSize) gstate = return gstate {_windowSize = newSize}
input (EventKey (Char 'q') _ _ _) gstate = exitSuccess
input e gstate
  | mapSelected ss = return $ gameInput e gstate
  | otherwise = return (selectInput e gstate)
  where
    ss = _selectState gstate

gameInput :: Event -> GameState -> GameState
gameInput (EventKey (SpecialKey key) Down _ _) gstate = case key of
  KeyUp -> gstate {_pacMan = pacMan {nextDirection = Just N}}
  KeyLeft -> gstate {_pacMan = pacMan {nextDirection = Just W}}
  KeyDown -> gstate {_pacMan = pacMan {nextDirection = Just S}}
  KeyRight -> gstate {_pacMan = pacMan {nextDirection = Just E}}
  KeySpace -> gstate {_pause = unpause (_pause gstate)}
  KeyEsc -> gstate {_pause = unpause (_pause gstate)}
  -- KeyEnd -> gameOverHandler gstate -- debug
  _ -> gstate
  where
    pacMan = _pacMan gstate
gameInput (EventKey (Char char) Down _ _) gstate = case char of
  'w' -> gstate {_pacMan = pacMan {nextDirection = Just N}}
  'a' -> gstate {_pacMan = pacMan {nextDirection = Just W}}
  's' -> gstate {_pacMan = pacMan {nextDirection = Just S}}
  'd' -> gstate {_pacMan = pacMan {nextDirection = Just E}}
  'p' -> gstate {_pause = unpause (_pause gstate)}
  'r' -> getInitialState gstate
  -- 'c' -> addCherryToState gstate
  _ -> gstate
  where
    pacMan = _pacMan gstate
gameInput _ gstate = gstate -- Otherwise keep the same

selectInput :: Event -> GameState -> GameState
selectInput (EventKey (SpecialKey key) Down _ _) gstate = case key of
  KeyLeft -> gstate {_selectState = s {selectIndex = i (-1)}}
  KeyRight -> gstate {_selectState = s {selectIndex = i 1}}
  KeyEnter -> selectMap gstate
  _ -> gstate
  where
    s = _selectState gstate
    i min = wrapAround s (selectIndex s + 1 * min)

    wrapAround :: SelectState -> Int -> Int
    wrapAround s i
      | i >= len = 0
      | i == -1 = len - 1
      | otherwise = i
      where
        len = length $ maps s

    selectMap :: GameState -> GameState
    selectMap gstate@GameState {_images, _windowSize, _selectState, _highScoreData} =
      (initialState g _images _windowSize (_selectState {mapSelected = True}) pLoc sLoc _highScoreData False) {_pause = Play}
      where
        pLoc = fromMaybe (0, 0) (getPrisonLocaction g 0)
        sLoc = fromMaybe (0, 0) (getSpawnLocation g 0)
        g = getGrid _selectState
selectInput e gstate = gstate
