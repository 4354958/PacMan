module Main where

import Controller
import qualified Data.Bifunctor
import Data.Char (toLower)
import Data.Maybe (fromMaybe)
import GHC.Float (int2Float)
import GHC.IO.Handle.FD (openFile)
import GHC.IO.IOMode (IOMode (ReadMode))
import Graphics.Gloss (BitmapData (bitmapSize), BitmapFormat, loadBMP)
import Graphics.Gloss.Interface.Environment (getScreenSize)
import Graphics.Gloss.Interface.IO.Game
  ( Display (InWindow),
    black,
    playIO,
  )
import Grid (getPrisonLocaction, getSpawnLocation)
import Input
import Load
import Model
import System.Directory (copyFile, doesFileExist)
import System.Directory.Internal.Prelude (exitFailure)
import System.FilePath.Posix (takeFileName)
import View

main :: IO ()
main = do
  importCustomMap
  imgs <- loadBitmaps
  maps <- loadMaps
  highscoreData <- loadHighScores maps

  putStrLn "\n\ESC[0mstarting game..."

  let initScreenSize = (800, 800)
  let grid = snd $ head maps -- default for init state
  let ss = initialSelectState maps
  let prisonLocation = fromMaybe (0, 0) (getPrisonLocaction grid 0)
  let spawnLocation = fromMaybe (0, 0) (getSpawnLocation grid 0)
  let state = initialState grid imgs initScreenSize ss prisonLocation spawnLocation highscoreData False

  playIO
    (InWindow "PacMan!" initScreenSize (0, 0)) -- Or FullScreen
    black -- Background color
    45 -- Frames per second
    state -- Initial state
    view -- View function
    input -- Event function
    step -- Step function