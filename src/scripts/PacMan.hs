module PacMan where

import Grid
import Movement

data PacMan = PacMan
  { _currentLocation :: GridLocation,
    _absoluteLocation :: AbsoluteLocation,
    currentDirection :: CurrentDirection,
    nextDirection :: NextDirection
  }

instance Entity PacMan where
  updateGridLocation _grid (PacMan currentLocation absl cd newDirection) = case tryMoveInDirection _grid currentLocation False newDirection of
    Just newLocation -> PacMan newLocation newAbsLocation newDirection Nothing -- if the player
    Nothing -> case tryMoveInDirection _grid currentLocation False cd of
      Just newLocation -> PacMan newLocation newAbsLocation cd newDirection
      Nothing -> PacMan currentLocation newAbsLocation Nothing Nothing
    where
      newAbsLocation = loc2AbsLoc currentLocation
  getGridLocation = _currentLocation
  getDirection = currentDirection
  getAbsLocation = _absoluteLocation
  setAbsLocation pacMan absLocation = pacMan {_absoluteLocation = absLocation}

defaultPacMan :: GridLocation -> PacMan
defaultPacMan currentLocation =
  PacMan
    { _currentLocation = currentLocation,
      _absoluteLocation = loc2AbsLoc currentLocation,
      currentDirection = Nothing,
      nextDirection = Nothing
    }