{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

module Load where

import Control.Monad (forM)
import Data.Aeson (decode, encode)
import qualified Data.ByteString.Lazy as B
import Data.List (isSuffixOf)
import Data.Maybe (fromMaybe)
import Graphics.Gloss
import Grid
import HighScore
import System.Directory (copyFile, doesFileExist, listDirectory)
import System.Directory.Internal.Prelude (catMaybes, toLower)
import System.FilePath (dropExtension, takeFileName)

-- load bitmap files from images folder and return as (fileName, Picture) tuples
loadBitmaps :: IO [(String, Picture)]
loadBitmaps = do
  putStrLn "\n\ESC[0mLoading images..."

  -- Get a list of files in the folder
  files <- listDirectory "src/images"

  -- make sure the file format is correct
  let bitmapFiles = filter (isSuffixOf ".bmp") files

  -- Load each bitmap file, convert it to a Picture, and return as a tuple
  mapM
    ( \file -> do
        pic <- loadBMP ("src/images/" ++ file)
        putStrLn $ prependFileName file "\ESC[92mSuccesfully loaded "
        return (dropExtension file, pic)
    )
    bitmapFiles

-- Function to load bitmap files from a folder and return as (fileName, Picture) tuples
loadMaps :: IO [(String, Grid)]
loadMaps = do
  putStrLn "\n\ESC[0mLoading maps..."

  -- Get a list of txt files
  files <- listDirectory "src/maps"

  let txtFiles = filter (isSuffixOf ".txt") files

  -- Load each txt file, and return as a tuple
  maybeMaps <- forM txtFiles $
    \file -> do
      (grid, errs) <- loadMap ("src/maps/" ++ file)
      if any (\case Fatal _ -> True; _ -> False) errs
        then do
          putStrLn $ prependFileName file "\ESC[31mMap skipped "
          return Nothing
        else do
          putStrLn $ prependFileName file "\ESC[92mSuccesfully loaded "
          return $ Just (dropExtension file, grid)
  return $ catMaybes maybeMaps

loadMap :: String -> IO (Grid, [ErrorMessage])
loadMap path = do
  newMap <- readFile path
  let file = takeFileName path
  let (grid, errs) = correctGrid $ readGrid newMap
  let errString = map (prependFileName file . show) errs
  mapM_ putStrLn errString
  return (grid, errs)

loadHighScores :: [(String, Grid)] -> IO HighscoreData
loadHighScores mps = do
  putStrLn "\n\ESC[0mLoading highscores..."
  jsonData <- B.readFile "src/highscores.json"
  case decode jsonData of
    Nothing -> do
      putStrLn "\ESC[31mError parsing highscores"
      return $ HighscoreData []
    Just hsData -> do
      putStrLn "\ESC[92mSuccesfully loaded highscores"
      let newHsData = addMissingMaps mps hsData
      saveHighScores newHsData
      return newHsData

saveHighScores :: HighscoreData -> IO ()
saveHighScores hsd = B.writeFile "src/highscores.json" (encode hsd)

-- importing a custom map
importCustomMap :: IO ()
importCustomMap = do
  doImport <- yesOrNo "\n\ESC[0mDo you wish to import a custom map?"
  if doImport
    then do getPath
    else putStrLn "\n\ESC[0mstarting game..."

yesOrNo :: String -> IO Bool
yesOrNo s = do
  putStrLn (s ++ " (y/n)")
  i <- getLine
  let input = map toLower i
  case map toLower i of
    "y" -> return True
    "n" -> return False
    _ -> do
      putStrLn "\ESC[0manswer with 'n' or 'y'"
      yesOrNo s

getPath :: IO ()
getPath = do
  putStrLn "\n\ESC[90m'q' to escape"
  putStrLn "\ESC[0mFilePath to custom map file: (You can drag and drop a txt file into the terminal)"
  i <- getLine
  putStrLn "\n"
  if i == "q" -- return from selection
    then putStrLn "\ESC[0mQuitting..."
    else do
      let path = filter (/= '\'') i
      let fileName = takeFileName path
      exists <- doesFileExist path
      if exists
        then do
          putStrLn "\ESC[92mFile found"
          (g, errs) <- loadMap path
          if any (\case Fatal _ -> True; _ -> False) errs
            then do
              putStrLn $ prependFileName fileName "\ESC[31mIllegal map - fix before importing"
            else do
              putStrLn $ prependFileName fileName "\ESC[92mSuccesfully loaded"
              putStrLn "\ESC[0mYou should now be able to find the map in the \ESC[96mselection menu!"
              copyFile path ("src/maps/" ++ fileName)
        else do
          putStrLn "\ESC[31mFile not found"
          getPath
      importCustomMap

prependFileName :: String -> String -> String
prependFileName file = (++) ("\ESC[0m[" ++ dropExtension file ++ "]")

-- When no file is found, replace it by a green square
notFound :: Picture
notFound = color green $ polygon [(0.0, 0.0), (0.0, -8.0), (8.0, -8.0), (8.0, 0.0)]
