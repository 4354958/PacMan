{-# LANGUAGE NamedFieldPuns #-}

-- | This module contains the data types
--   which represent the state of the game
module Model where

import Ghost
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game (Event)
import Grid
import HighScore (HighscoreData)
import PacMan

type Score = Int

type Lives = Int

type Frames = Int

data EatingEvent = EatingEvent
  { _isEnabled :: Bool,
    _framesLeft :: Frames
  }

data Pause = Pause | Play

data FreezeEvent = FreezeEvent EventType Frames

data EventType = NoEvent | GameStart | GameOver | GameWon | GhostEaten | PacManDeath
  deriving (Show)

data SelectState = Select
  { mapSelected :: Bool,
    maps :: [(String, Grid)],
    selectIndex :: Int
  }

getMapName :: SelectState -> String
getMapName s = fst $ maps s !! selectIndex s

getGrid :: SelectState -> Grid
getGrid s = snd $ maps s !! selectIndex s

data GameState = GameState
  { _elapsedTime :: Float,
    _frameCounter :: Int,
    _animationFrameCount :: Int,
    _freezeEvent :: FreezeEvent,
    _pause :: Pause,
    _images :: [(String, Picture)],
    _selectState :: SelectState,
    _windowSize :: (Int, Int),
    _grid :: Grid,
    _lives :: Lives,
    _score :: Score,
    _pacMan :: PacMan,
    _ghosts :: [Ghost],
    _eatingEvent :: EatingEvent,
    _prisonLocation :: GridLocation,
    _pmSpawnLocation :: GridLocation,
    _highScoreData :: HighscoreData,
    _generatedCherry :: Bool
  }

unpause :: Pause -> Pause
unpause Pause = Play
unpause Play = Pause

modifyScore :: GameState -> Score -> GameState
modifyScore gstate s = gstate {_score = _score gstate + s}

causeFreezeEvent :: GameState -> EventType -> Frames -> GameState
causeFreezeEvent gstate et f = gstate {_freezeEvent = FreezeEvent et f}

addCherryToState :: GameState -> IO GameState
addCherryToState gstate = do
  location <- selectRandomGridLocation $ getAllLocationsOf (_grid gstate) [Dot, Empty]
  -- putStrLn $ "Got random location of" ++ show location
  -- putStrLn $ "Current grid is " ++ show (_grid gstate)
  return $ toCherry gstate location
  where
    toCherry gstate gl = gstate {_grid = replaceInGrid (_grid gstate) (gl, Cherry)}

initialState :: Grid -> [(String, Picture)] -> (Int, Int) -> SelectState -> GridLocation -> GridLocation -> HighscoreData -> Bool -> GameState
initialState grid imgs wSize selectState prisLoc@(x, y) spawnLoc hsd hasCherry =
  GameState
    { _elapsedTime = 0,
      _frameCounter = 0,
      _animationFrameCount = 0,
      _freezeEvent = FreezeEvent GameStart 30,
      _pause = Play,
      _images = imgs,
      _selectState = selectState,
      _windowSize = wSize,
      _grid = grid,
      _lives = 3,
      _score = 0,
      _pacMan = defaultPacMan spawnLoc,
      _ghosts =
        [ defaultBlinky (x, y), -- dont reorder, this order expected to be hardcoded by Controller.hs adjustghosts
          defaultClyde (x + 1, y),
          defaultPinky (x + 2, y),
          defaultInky (x + 3, y)
        ],
      _eatingEvent = EatingEvent False 0,
      _prisonLocation = prisLoc,
      _pmSpawnLocation = spawnLoc,
      _highScoreData = hsd,
      _generatedCherry = hasCherry
    }

getInitialState :: GameState -> GameState
getInitialState GameState {_images, _selectState, _windowSize, _prisonLocation, _pmSpawnLocation, _highScoreData} =
  initialState (getGrid _selectState) _images _windowSize _selectState _prisonLocation _pmSpawnLocation _highScoreData False

initialSelectState :: [(String, Grid)] -> SelectState
initialSelectState maps = Select False maps 0
